import 'dart:async';
import 'package:http/http.dart' as http;


class Code {
  final int id;
  final String qRcode;
  final String code;
  final String dateDebut;
  final String dateFin;
  final String description;

  Code._({this.id,this.qRcode,this.code,this.description,this.dateDebut,this.dateFin});
  Code({this.id,this.qRcode,this.code,this.description,this.dateDebut,this.dateFin});


  factory Code.fromJson(Map<String, dynamic> json){
    return new Code._(
      id: json['id'],
      code: json['code'],
      qRcode: json['qRcode'],
      description: json['description'],
      dateDebut: json['dateDebut'],
      dateFin: json['dateFin']
    );
  }

  @override
  String toString() {
    return 'Code{id: $id, qRcode: $qRcode, code: $code, dateDebut: $dateDebut, dateFin: $dateFin, description: $description}';
  }


}